import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from "./router"
import './registerServiceWorker'

import "@/assets/styles/master.css"
import "@/assets/styles/bootstrap.min.css"
import "@/assets/styles/fontawesome.min.css"
import "@/assets/styles/brands.css"
import "@/assets/styles/light.min.css"
import "@/assets/styles/solid.min.css"
import "@/assets/styles/vuetify.css"

Vue.prototype.$tempGlobal = {}
Vue.prototype.$computingPriceOffer = function (price, off) {
  let priceNum = price;
  if (typeof priceNum !== "number") {
    priceNum = price.replace(",", "");
  }
  return this.$numberWithCommas(parseInt(priceNum) - (parseInt(priceNum) * (off / 100)));
};


Vue.prototype.$tempGlobal.storeDetail = [

  {
     detailData: [{
      tab: 'تزریقات',
      content: [{
        id: 1,
        src: 'syringe.png',
        title: 'تزریقات (تنها یه تزریق)',
        category: 'خدمات پرستاری',
        subcategory: 'تزریقات',
        discount: 0,
        num: 0
      },
        {
          id: 2,
          src: 'syringe.png',
          title: 'تزریقات 2 (تنها یه تزریق)',
          category: 'خدمات پرستاری',
          subcategory: 'تزریقات',
          price: 144000,
          discount: 10,
          num: 0
        },]
    },
      {
      tab: 'سرم تراپی',
      content: [{
        id: 1,
        src: 'syringe.png',
        title: 'تزریقات (تنها یه تزریق)',
        category: 'خدمات پرستاری',
        subcategory: 'تزریقات',
        discount: 0,
        num: 0
      },
        {
          id: 2,
          src: 'syringe.png',
          title: 'تزریقات 2 (تنها یه تزریق)',
          category: 'خدمات پرستاری',
          subcategory: 'تزریقات',
          price: 144000,
          discount: 10,
          num: 0
        },]
    },]
  },

];



Vue.config.productionTip = false
Vue.prototype.$numberWithCommas = function (x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
Vue.prototype.$global = {};
new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
