import Vue from 'vue'
import VueRouter from 'vue-router'
import Router from 'vue-router'
import Home from "./components/Home"
import NursingServices from "./components/NursingServices"
import Maintenance from "./components/Maintenance"
import MedicalStore from "./components/MedicalStore"
import TherapyServices from "./components/TherapyServices"
import MedicineDelivery from "./components/MedicineDelivery"
import Login from "./components/Login"
import Register from "./components/Register"
import ProductList from "./components/ProductList"
import ProductDetails from "./components/ProductDetails"
import NursingServiceDetails from "./components/NursingServiceDetails"
import Profile from "./components/Profile";



Vue.use(VueRouter);
export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/Login'
        },
        {
            path: "/Home",
            component: Home,
        },
        {
            path: "/NursingServices",
            component: NursingServices,
        },
        {
            path: "/Maintenance",
            component: Maintenance,
        },
        {
            path: "/MedicalStore",
            component: MedicalStore,
        },
        {
            path: "/TherapyServices",
            component: TherapyServices,
        },

        {
            path: "/MedicineDelivery",
            component: MedicineDelivery,
        },
        {
            path: "/Login",
            component: Login,
        },
        {
            path: "/Register",
            component: Register,
        },
        {
            path: "/ProductList",
            component: ProductList,
        },
        {
            path: "/ProductDetails",
            component: ProductDetails,
        },
        {
            path: "/NursingServiceDetails",
            component: NursingServiceDetails,
        },
        {
            path: "/Profile",
            component: Profile,
        }
    ]
});
